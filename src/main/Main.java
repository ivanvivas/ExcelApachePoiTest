package main;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;

import excel.LibroExcel;
import util.EstilosExcel;

public class Main {
	private static List<String> headersList = new ArrayList<>();
	private static List<Integer> dataList = new ArrayList<>();
	
	public static void main(String[] args) {
		llenarData();
		
		System.out.println("Creando libro");
		LibroExcel libro = new LibroExcel("libroNuevo");
		
		System.out.println("Creando hoja 1");
		libro.insertarNuevaHoja("Hoja1");
		
		System.out.println("Añadiendo titulos y datos");
		try {
			Sheet hoja = libro.obtenerHoja("Hoja1");
			hoja = EstilosExcel.insertarEstiloDeHoja(hoja);
			hoja = EstilosExcel.insertarAnchoColumnas(hoja, 5);
			libro.insertarFila(headersList, hoja, 7, EstilosExcel.ESTILO_CABECERA);
			libro.insertarFila(dataList, hoja, 8, EstilosExcel.ESTILO_CONTENIDO);
			libro.insertarFila(dataList, hoja, 9, EstilosExcel.ESTILO_CONTENIDO);
			libro.insertarFila(dataList, hoja, 10, EstilosExcel.ESTILO_CONTENIDO);
			libro.insertarFila(dataList, hoja, 11, EstilosExcel.ESTILO_CONTENIDO);
			libro.insertarFila(dataList, hoja, 12, EstilosExcel.ESTILO_CONTENIDO);
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
		
		System.out.println("Insertar imagen");
		try {
			Sheet hoja = libro.obtenerHoja("Hoja1");
			libro.insertarImagen("img/logo-bci.jpg", hoja);
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
		
//		System.out.println("Creando hoja 2");
//		libro.insertarNuevaHoja("Hoja2");
//		
//		System.out.println("Añadiendo titulos y datos");
//		try {
//			Sheet hoja = libro.obtenerHoja("Hoja2");
//			libro.insertarFila(headersList, hoja, 0);
//			libro.insertarFila(dataList, hoja, 1);
//			libro.insertarFila(dataList, hoja, 2);
//			libro.insertarFila(dataList, hoja, 3);
//			libro.insertarFila(dataList, hoja, 4);
//			libro.insertarFila(dataList, hoja, 5);
//		} catch (Exception e) {
//			System.out.println("Error: " + e.getMessage());
//		}
		
		System.out.println("Escribir libro");
		libro.escribirLibro();
	}
	
	private static void llenarData() {
		for (int i=0; i<5; i++) {
			headersList.add("Titulo " + (i+1));
			dataList.add((i+1));
		}
	}
}
