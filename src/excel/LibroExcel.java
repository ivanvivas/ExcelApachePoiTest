package excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import util.EstilosExcel;

public class LibroExcel {
	/**
	 * Libro de excel
	 */
	private Workbook libro;
	
	/**
	 * Archivo de salida
	 */
	private OutputStream archivoSalida;
	
	/**
	 * Mapa con estilos para cabeceras y datos
	 */
	private final Map<String, CellStyle> estilosMap;
	
	private CreationHelper createHelper;
	
	/**
	 * Constructor
	 */
	public LibroExcel() {
		/* Crear libro */
		this.libro = new XSSFWorkbook();
		
		/* CreatioHelper para la creacion de celdas */
		this.createHelper = libro.getCreationHelper();
		
		/* Crear archivo de salida */
		try {
			this.archivoSalida = new FileOutputStream("libroExcel.xlsx");
	    } catch (FileNotFoundException  e) {
			System.out.println("Error: " + e.getMessage());
		}
		
		this.estilosMap = EstilosExcel.crearEstilos(libro);
	}
	
	/**
	 * Constructor parametrico
	 */
	public LibroExcel(String nombreArchivo) {
		/* Crear libro */
		this.libro = new XSSFWorkbook();
		
		/* CreatioHelper para la creacion de celdas */
		this.createHelper = libro.getCreationHelper();
		
		/* Crear archivo de salida */
		try {
			this.archivoSalida = new FileOutputStream(nombreArchivo + ".xlsx");
	    } catch (FileNotFoundException  e) {
			System.out.println("Error: " + e.getMessage());
		}
		
		this.estilosMap = EstilosExcel.crearEstilos(libro);
	}
	
	public void insertarNuevaHoja(String titulo) {
		this.libro.createSheet(titulo);
	}
	
	public Sheet obtenerHoja(String titulo) {
		Sheet hoja = libro.getSheet(titulo);
		
		return hoja;
	}
	
	public void insertarLibro(Workbook libro) {
		this.libro = libro;
	}
	
	public void setArchivoSalida(String nombreArchivo) {
		try {
			this.archivoSalida = new FileOutputStream(nombreArchivo + ".xlsx");
	    } catch (FileNotFoundException  e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	public void escribirLibro() {
		try {
			this.libro.write(archivoSalida);
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	@SuppressWarnings("hiding")
	public <T> Sheet insertarFila(List<T> dataList, Sheet hoja, int fila, String estilo) throws Exception {
		if (!dataList.isEmpty() && hoja != null) {
			/* Crear fila */
			Row row = hoja.createRow(fila);
			row.setHeightInPoints(20);
			
			/* Añadir celdas a la fila */
			for (int i=0; i<dataList.size(); i++) {
				Cell celdaTmp = row.createCell(i); 
				celdaTmp.setCellValue(createHelper.createRichTextString(dataList.get(i).toString()));
				celdaTmp.setCellStyle(estilosMap.get(estilo));
			}
		}
		else {
			throw new Exception("Error al insertar datos en fila");
		}
		
		return hoja;
	}
	
	public Sheet insertarImagen(String nombreImagen, Sheet hoja) throws Exception {
		if (hoja != null) {
			InputStream is = new FileInputStream(nombreImagen);
		    byte[] bytes = IOUtils.toByteArray(is);
		    int pictureIdx = libro.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
		    is.close();
		    
		    // Create the drawing patriarch.  This is the top level container for all shapes. 
		    Drawing<?> drawing = hoja.createDrawingPatriarch();

		    //add a picture shape
		    ClientAnchor anchor = createHelper.createClientAnchor();
		    //set top-left corner of the picture,
		    //subsequent call of Picture#resize() will operate relative to it
		    anchor.setCol1(0);
		    anchor.setRow1(0);
		    Picture pict = drawing.createPicture(anchor, pictureIdx);

		    //auto-size picture relative to its top-left corner
		    pict.resize();
		}
		else {
			throw new Exception("Error al insertar imagen");
		}
		
		return hoja;
	}
}
