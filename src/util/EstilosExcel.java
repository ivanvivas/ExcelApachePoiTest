package util;

import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;

public class EstilosExcel {
	
	private final static int ANCHO_COLUMNA = 20; //the column is 15 characters wide
	
	public final static String ESTILO_CABECERA = "cabecera";
	public final static String ESTILO_CONTENIDO = "contenido";
	
	public static Sheet insertarEstiloDeHoja(Sheet hoja) {
		hoja.setPrintGridlines(false);
		hoja.setDisplayGridlines(false);
        PrintSetup printSetup = hoja.getPrintSetup();
        printSetup.setLandscape(true);
        hoja.setFitToPage(true);
        hoja.setHorizontallyCenter(true);
        hoja.setFitToPage(true);
        hoja.setHorizontallyCenter(true);
        
		return hoja;
	}
	
	public static Sheet insertarAnchoColumnas(Sheet hoja, int columnas) {
		for (int i=0; i<columnas; i++) {
            hoja.setColumnWidth(i, 256 * ANCHO_COLUMNA);
        }
      
		return hoja;
	}
	
	/**
	 * Metodo para crear y almacenar en map los estilos basicos de las celdas de la hooja
	 * 
	 * @return
	 */
	public static Map<String, CellStyle> crearEstilos(Workbook libro){
        Map<String, CellStyle> stylesAux = new HashMap<>();
        
        CellStyle estilo1 = libro.createCellStyle();
        Font headersFont = libro.createFont();
        headersFont.setFontHeightInPoints((short)12);
        headersFont.setBold(true);
        estilo1.setFont(headersFont);
        estilo1.setBorderBottom(BorderStyle.THIN);
        estilo1.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        estilo1.setBorderLeft(BorderStyle.THIN);
        estilo1.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        estilo1.setBorderRight(BorderStyle.THIN);
        estilo1.setRightBorderColor(IndexedColors.BLACK.getIndex());
        estilo1.setBorderTop(BorderStyle.THIN);
        estilo1.setTopBorderColor(IndexedColors.BLACK.getIndex());
        estilo1.setAlignment(HorizontalAlignment.CENTER);
        estilo1.setFillForegroundColor(IndexedColors.CORNFLOWER_BLUE.getIndex());
        estilo1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        estilo1.setVerticalAlignment(VerticalAlignment.CENTER);
        estilo1.setWrapText(true);
        stylesAux.put(ESTILO_CABECERA, estilo1);
        

        CellStyle estilo2 = libro.createCellStyle();
        Font contentFont = libro.createFont();
        contentFont.setFontHeightInPoints((short)10);
        estilo2.setFont(contentFont);
        estilo2.setBorderBottom(BorderStyle.THIN);
        estilo2.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        estilo2.setBorderLeft(BorderStyle.THIN);
        estilo2.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        estilo2.setBorderRight(BorderStyle.THIN);
        estilo2.setRightBorderColor(IndexedColors.BLACK.getIndex());
        estilo2.setBorderTop(BorderStyle.THIN);
        estilo2.setTopBorderColor(IndexedColors.BLACK.getIndex());
        estilo2.setAlignment(HorizontalAlignment.LEFT);
        estilo2.setWrapText(true);
        stylesAux.put(ESTILO_CONTENIDO, estilo2);
        
        return stylesAux;
    }
}
